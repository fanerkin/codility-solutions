function solution(N) {
    var i,
        p = Infinity;

    function setP(n) {
        p = Math.min( p, 2 * ( n + N / n ) );
    }

    for ( i = 1; i * i <= N; i++ ) {
        if ( N % i === 0 ) setP(i);
    }

    return p;
}
