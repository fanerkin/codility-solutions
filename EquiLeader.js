function solution(A) {
    var a = findLeader(A),
        b = { length: 0, total: 0 },
        s = 0;

    if ( a === null || a.total * 2 < A.length + 1 ) return 0;

    for ( i = 0; i < A.length; i++ ) {
        a.length--;
        b.length++;
        if ( A[i] == a.leader ) {
            a.total--;
            b.total++;
        }
        if ( hasLeader(a) && hasLeader(b) ) s++;
    }

    return s;

    function hasLeader(a) {
        return a.total * 2 > a.length;
    }

    function findLeader(A) {
        var i,
            top = null,
            l = 0;

        for ( i = 0; i < A.length; i++ ) {
            if ( top === null ) {
                top = A[i];
                l++;
            } else if ( A[i] == top ) {
                l++;
            } else {
                l--;
                if (!l) top = null;
            }
        }

        if ( top === null ) return null;
        for ( i = 0, l = 0; i < A.length; i++) {
            if ( A[i] == top ) l++;
        }
        return l * 2 > A.length ?
            { leader: top, total: l, length: A.length } :
            null;
    }
}
