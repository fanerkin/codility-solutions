function solution(A) {
    var sums = [0],
        i, p, q, sum,
        l = A.length;

    for ( i = 0; i < l; i++ ) {
        sum += A[i];
        sums.push(sum);
    }

    function getAverage(p, q) {
        return (sums[q] - sums[p]) / (q - p);
    }


}
