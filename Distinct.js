function solution(A) {
    var u = [],
        i;

    A.sort();
    for ( i = 0; i < A.length; i++ ) {
        if (A[i] !== u[u.length - 1]) u.push(A[i]);
    }
    return u.length;
}
