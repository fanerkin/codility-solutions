function solution(A) {
    var sum = A.reduce(function (b, el) {return b + el}, 0),
        sumLeft = 0,
        i,
        diff,
        diffMin = Infinity;

    for ( i = 0; i < A.length - 1; i++ ) {
        sum -= A[i];
        sumLeft += A[i];
        diff = Math.abs(sum - sumLeft);
        diffMin = Math.min( diff, diffMin );
    }

    return diffMin;
}
