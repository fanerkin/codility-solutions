function solution(N, A) {
    var B = createEmptyArray(N),
        i,
        el,
        max = 0;

    function createEmptyArray(N) {
        return new Array(N + 1).join('0').split('').map(Number);
    }

    function inc(i) {
        B[i]++;
    }

    function setMax(B, max) {
        var l = B.length;

        while (l--) {
            B[l] = max;
        }
    }

    for ( i = 0; i < A.length; i++ ) {
        el = A[i] - 1;
        if ( el == N ) setMax(B, max);
        else {
            inc(el);
            if ( B[el] > max ) max = B[el];
        }
    }
    return B;
}
