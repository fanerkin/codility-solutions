function solution(A) {
    var n = 0,
        i, j,
        l = A.length,
        B;

    function intersect(a, b) {
        return B[b][0] <= B[a][1];
    }

    if ( l < 2 ) return 0;

    B = A
        .map(function (el, i) {return [ i - el, i + el ] } )
        .sort(function (a, b) {return a[0] - b[0]} );

    for ( i = 0; i < l - 1; i++ ) {
        for ( j = i + 1; j < l; j++ ) {
            if ( intersect(i, j) ) {
                n++;
                if ( n > 1e7 ) return -1;
            } else break;
        }
    }

    return n;
}
