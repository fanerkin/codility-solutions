function solution(A) {
    var candidates = [],
        l = A.length;

    A.sort(function (a, b) { return a - b });
    // three last
    candidates.push( A[l - 1] * A[l - 2] * A[l - 3] );
    // two first and last, to capture 2 big negatives
    candidates.push( A[0] * A[1] * A[l - 1] );

    return Math.max.apply(null, candidates);
}
