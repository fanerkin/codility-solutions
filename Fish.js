function solution(A, B) {
    var i,
        size,
        downs = [],
        eaten = 0;

    for ( i = 0; i < A.length; i++ ) {
        if ( B[i] == 1 ) {
            downs.push(A[i]);
            continue;
        } else {
            while ( size = downs.pop() ) {
                eaten++;
                if ( size > A[i] ) {
                    downs.push(size);
                    break;
                }
            }
        }
    }
    return A.length - eaten;
}
