#!/bin/bash

files="$(ls | grep .txt)"
for f in $files
do
    echo $f
    echo https://codility.com/demo/results/$(sed -n '1,2p' $f)
    echo
done
echo -n total=
echo "$files" | wc -l
