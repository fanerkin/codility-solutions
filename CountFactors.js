function solution(N) {
    var i,
        n = 0;

    for ( i = 1; i * i < N; i++ ) {
        if ( N % i === 0 ) n++;
    }

    n *= 2;
    if ( i * i == N ) n += 1;

    return n;
}
