function solution(A) {
    var B = [],
        i;

    A.forEach( function (el) { if (el > 0) B[el] = true;} );

    for ( i = 1; ; i++ ) {
        if (!B[i]) return i;
    }
}
