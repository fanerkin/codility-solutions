function solution(S, P, Q) {
    var
        indexes = {
            A: {},
            C: {},
            G: {},
            T: {}
        },

        i,
        min,
        results = [];

    S.split('').forEach(function (el, i) { indexes[el][i] = true; });

    for ( i = 0; i < P.length; i++ ) {
        min = 4;
        for ( ; P[i] <= Q[i]; P[i]++ ) {
            if (indexes['A'][P[i]]) {
                min = 1;
                break;
            }
            if ( min > 2 && indexes['C'][P[i]]) min = 2;
            if ( min > 3 && indexes['G'][P[i]]) min = 3;
        }
        results.push(min);
    }
    return results;
}
