function solution(H) {
    var i,
        blocks = 0,
        s = [0];

    for ( i = 0; i < H.length; i++ ) {
        while ( H[i] < s[s.length - 1] ) {
            s.pop();
        }
        if ( H[i] > s[s.length - 1] ) {
            s.push(H[i]);
            blocks++;
        }
    }

    return blocks;
}
