function solution(X, A) {
    var filled = [],
        i,
        left = X;

    for ( i = 0; i < A.length; i++ ) {
        if ( !filled[A[i]] ) {
            filled[A[i]] = true;
            left--;
        }
        if (!left) return i;
    }

    return -1;
}
