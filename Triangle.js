function solution(A) {
    var positives = A.filter(function (el) {return el > 0;});

    function isThirdEdgeTooLong(a, b, c) {
        return a + b <= c;
    }

    function isTwoFirstEdgesFit(a, b, c) {
        return a + c > b &&
            b + c > a;
    }

    function hasTriangle(A) {
        var i, j, k,
            l = A.length;

        A.sort();
        for ( i = 0; i < l - 2; i++ ) {
            for ( j = i + 1; j < l - 1; j++ ) {
                for ( k = j + 1; k < l; k++ ) {
                    if ( isThirdEdgeTooLong( A[i], A[j], A[k] ) ) break;
                    if ( isTwoFirstEdgesFit( A[i], A[j], A[k] ) ) return 1;
                }
            }
        }
        return 0;
    }

    return hasTriangle(positives);
}
