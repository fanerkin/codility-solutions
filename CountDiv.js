function solution(A, B, K) {
    var x = A % K;

    if (x) A += K - x;
    if (A > B) return 0;

    return Math.floor( (B - A) / K ) + 1;
}
