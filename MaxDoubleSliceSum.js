function solution(A) {
    var mSlice = 0,
        mEnding = 0,
        min = A[1],
        i;

    for ( i = 2; i < A.length - 1; i++ ) {
        if ( A[i] < min ) {
            mEnding += min;
            min = A[i];
        } else mEnding += A[i];
        if ( mEnding < 0 ) {
            mEnding = 0;
            min = 0;
        }

        mSlice = Math.max( mSlice, mEnding );
    }

    return mSlice;
}
