function solution(A) {
    var d = getData(A),
        i;

    function getData(A) {
        var i = A.length,
            s = {},
            items = 0;

        while (i--) {
            if (!s[A[i]]) {
                s[A[i]] = 1;
                items++;
            }
        }

        return {s: s, i: items};
    }

    for ( i = 0; i < A.length; i++) {
        if (d.s[A[i]]) {
            d.s[A[i]] = null;
            d.i--;
        }

        if (!d.i) {
            return i;
        }
    }
}
