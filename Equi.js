function solution(A) {
    var l = A.length,
        i = l,
        s = sum(A),
        p = 0;

    function sum(A) {
        var i = A.length,
            s = 0;
        while (i--) {
            s += A[i];
        }
        return s;
    }
    while (i--) {
        s -= A[i];
        if ( s == p ) return i;
        else p += A[i];
    }
    return -1;
}

