function solution(A) {
    var B = [],
        i;

    for ( i = 1; i < A.length; i++ ) {
        B.push(A[i] - A[i - 1]);
    }

    function maxSlice(A) {
        var mSlice = 0,
            mEnding = 0,
            i = 0;

        for ( i = 0; i < A.length; i++ ) {
            mEnding = Math.max( 0, mEnding + A[i] );
            mSlice = Math.max( mEnding, mSlice );
        }

        return mSlice;
    }

    return maxSlice(B);
}
