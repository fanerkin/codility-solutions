function solution(A) {
    var i = A.length,
        passed = 0,
        west = 0;

    while (i--) {
        if (A[i] == 1) west++;
        else passed += west;
        if (passed > 1e9) return -1;
    }
    return passed;
}
