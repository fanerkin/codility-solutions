function solution(S) {
    var
        stack = [],
        opens = {
            '(': ')',
            '{': '}',
            '[': ']'
        },
        closes = {
            ')': '(',
            '}': '{',
            ']': '['
        },
        i;

    for ( i = 0; i < S.length; i++ ) {
        if ( opens[S[i]] ) stack.push(S[i]);
        else if ( stack.pop() != closes[S[i]] ) return 0;
    }
    return stack.length === 0 ? 1 : 0;
}
