function solution(A) {
    var mEnding = -Infinity,
        mSlice = -Infinity,
        i;

    for ( i = 0; i < A.length; i++ ) {
        mEnding = Math.max( mEnding + A[i], A[i] );
        mSlice = Math.max( mSlice, mEnding );
    }

    return mSlice;
}
