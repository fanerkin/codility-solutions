function solution(A) {
    var i = A.length,
        B = [];

    while (i--) {
        if (B[A[i]] || A[i] > A.length) return 0;
        B[A[i]] = true;
    }

    return 1;
}
