function solution(A) {
    var i,
        l = findCandidate(A),
        index = -1,
        items = 0;

    if ( l === null ) return -1;

    for ( i = 0; i < A.length; i++ ) {
        if ( A[i] == l ) {
            items++;
            if ( index == -1 ) index = i;
        }
    }

    return items * 2 > A.length ? index : -1;

    function findCandidate(A) {
        var i,
            s = [];

        for ( i = 0; i < A.length; i++ ) {
            if ( !s.length || A[i] == s[0] ) s.unshift(A[i]);
            else s.shift();
        }

        return s.length ? s[0] : null;
    }
}
