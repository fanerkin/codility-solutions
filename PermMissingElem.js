function solution(A) {
    var l = A.length + 1,
        sumExpected = l * (l + 1) / 2,
        sum = 0;

    l--;
    while (l--) {
        sum += A[l];
    }

    return sumExpected - sum;
}
