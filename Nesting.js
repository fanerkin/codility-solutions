function solution(S) {
    var opens = 0,
        i;

    for ( i = 0; i < S.length; i++ ) {
        if (S[i] == '(') opens++;
        else opens--;
        if (opens < 0) return 0;
    }
    return opens === 0 ? 1 : 0;
}
